#!/bin/bash
# A simple backup script to do nightly system backups\
# (or as often as you schedule in cron)

DAYSOLD="7"
BACKUPDIR="/backups"

# Remove uneeded backups
find $BACKUPDIR -mtime +$DAYSOLD -name "*$HOSTNAME*" -exec rm{} \;

# Create new backup
cd $BACKUPDIR
if test -f "$HOSTNAME-snapshot"; then
  tar --listed-incremental="$HOSTNAME-snapshot" -cvzf $HOSTNAME-`date +%b-%d-%Y`-i.tar.gz /home/
else
  tar --listed-incremental="$HOSTNAME-snapshot" --level=0 -cvzf $HOSTNAME-`date +%b-%d-%Y`-f.tar.gz /home/
fi
